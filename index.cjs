const path = require("path");
const csvFilePath = path.join(__dirname,'./src/data/WorldCupMatches.csv');
const csvFilePath1 =path.join(__dirname,'./src/data/WorldCupPlayers.csv');
const matchesPerCity = require(path.join(__dirname,'./src/server/matchesPerCity.cjs'));
const matchesWonPerTeam = require(path.join(__dirname,'./src/server/matchesWonPerTeam.cjs'));
const numberOfRedCards = require(path.join(__dirname,'./src/server/numberOfRedCards.cjs'));
const topTenPlayers = require(path.join(__dirname,'./src/server/topTenPlayers.cjs'));
const matchesPerTeamPerStage=require('./src/server/matchesPerTeamPerStage.cjs');
const matchesPerRefree=require('./src/server/refree.cjs');
const csv = require('csvtojson');
const fs = require("fs");


let copyofWorldCupMatches= [];
csv()
    .fromFile(csvFilePath)
    .then((WorldCupMatches) => {
//problem find stages for each team
let matchesperteamperstage=matchesPerTeamPerStage(WorldCupMatches);
fs.writeFileSync(path.join(__dirname, './src/public/output/stagesandmatchesPerTeam.json'), JSON.stringify(matchesperteamperstage), "utf-8")


// problem find matches per each Referee

       let matchesperrefree= matchesPerRefree(WorldCupMatches);
    
       fs.writeFileSync(path.join(__dirname, './src/public/output/refreeMatches.json'), JSON.stringify(matchesperrefree), "utf-8")

        copyofWorldCupMatches=[...WorldCupMatches];
//first Question number of matches played in each city
        let matchespercity = matchesPerCity(WorldCupMatches);
        fs.writeFileSync(path.join(__dirname, './src/public/output/matchesPerCity.json'), JSON.stringify(matchespercity), "utf-8")
        


//second Question number of matches won per team
        let matcheswonperteam = matchesWonPerTeam(WorldCupMatches);
        fs.writeFileSync(path.join(__dirname, './src/public/output/matchesWonPerTeam.json'), JSON.stringify(matcheswonperteam), "utf-8")


    })

csv()
    .fromFile(csvFilePath1)
    .then((WorldCupMatches) => {
//third question numberof Red car per teaam
        let numberofredcards = numberOfRedCards(WorldCupMatches, copyofWorldCupMatches,2014);
        fs.writeFileSync(path.join(__dirname, './src/public/output/numberOfRedCards.json'), JSON.stringify(numberofredcards), "utf-8")

        
//fourth question top ten players 

        let toptenplayers = topTenPlayers(WorldCupMatches);
        fs.writeFileSync(path.join(__dirname, './src/public/output/topTenPlayers.json'), JSON.stringify(toptenplayers), "utf-8")

        

    })
