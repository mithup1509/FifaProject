function matchesWonPerTeam(WorldCupMatches) {
  if (WorldCupMatches.length === undefined) {
    return;
  }

  let matcheswonperteam = WorldCupMatches.reduce((acc, cur) => {
    if (cur["Home Team Goals"] > cur["Away Team Goals"]) {
      if (acc[cur["Home Team Name"]]) {
        acc[cur["Home Team Name"]]++;
      } else {
        acc[cur["Home Team Name"]] = 1;
      }
    } else if (cur["Home Team Goals"] < cur["Away Team Goals"]) {
      if (acc[cur["Away Team Name"]]) {
        acc[cur["Away Team Name"]]++;
      } else {
        acc[cur["Away Team Name"]] = 1;
      }
    } else {
      let wincondition = cur["Win conditions"].slice(
        cur["Win conditions"].length - 7,
        cur["Win conditions"].length - 2
      );
      let arrayforwincondition = wincondition.split(" - ");

      if (arrayforwincondition[0] > arrayforwincondition[1]) {
        if (acc[cur["Home Team Name"]]) {
          acc[cur["Home Team Name"]]++;
        } else {
          acc[cur["Home Team Name"]] = 1;
        }
      } else {
        if (acc[cur["Away Team Name"]]) {
          acc[cur["Away Team Name"]]++;
        } else {
          acc[cur["Away Team Name"]] = 1;
        }
      }
    }

    return acc;
  }, {});

  delete matcheswonperteam[" "];
  delete matcheswonperteam[""];

  return matcheswonperteam;
}
module.exports = matchesWonPerTeam;
