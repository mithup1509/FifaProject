function matchesPerTeamPerStage(WorldCupMatches) {
  let allTeamandStages = WorldCupMatches.reduce((acc, cur) => {
    if (acc[cur["Home Team Name"]]) {
      let valueofthisKey = acc[cur["Home Team Name"]];
      if (valueofthisKey[cur.Stage]) {
        valueofthisKey[cur.Stage]++;
      } else {
        valueofthisKey[cur.Stage] = 1;
      }
    } else {
      let homeTeamStage = {};
      homeTeamStage[cur.Stage] = 1;
      acc[cur["Home Team Name"]] = homeTeamStage;
    }

    if (acc[cur["Away Team Name"]]) {
      let valueofthisKey = acc[cur["Away Team Name"]];
      if (valueofthisKey[cur.Stage]) {
        valueofthisKey[cur.Stage]++;
      } else {
        valueofthisKey[cur.Stage] = 1;
      }
    } else {
      let homeTeamStage = {};
      homeTeamStage[cur.Stage] = 1;
      acc[cur["Away Team Name"]] = homeTeamStage;
    }

    return acc;
  }, {});

  return allTeamandStages;
}
module.exports = matchesPerTeamPerStage;
