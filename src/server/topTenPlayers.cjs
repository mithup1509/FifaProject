function topTenPlayers(WorldCupPlayers) {
  if (WorldCupPlayers.length === undefined) {
    return;
  }
  let numberOfMatchesPerPlayer = WorldCupPlayers.reduce((acc, cur) => {
    if (acc[cur["Player Name"]]) {
      acc[cur["Player Name"]] += 1;
    } else {
      acc[cur["Player Name"]] = 1;
    }
    return acc;
  }, {});

  let numberofGoalsPerPlayer = WorldCupPlayers.filter(
    (players) => players.Event !== undefined
  )
    .filter((checkgoal) => checkgoal.Event.includes("G"))
    .reduce((acc, cur) => {
      if (acc[cur["Player Name"]]) {
        acc[cur["Player Name"]] += 1;
      } else {
        acc[cur["Player Name"]] = 1;
      }
      return acc;
    }, {});

  let averageGoalperPlayer = {};
  for (let val in numberofGoalsPerPlayer) {
    averageGoalperPlayer[val] =
      numberofGoalsPerPlayer[val] / numberOfMatchesPerPlayer[val];
  }
  const arrayforsort = Object.entries(averageGoalperPlayer);
  let sortedArray = arrayforsort
    .sort((firstValue, secondValue) => {
      return secondValue[1] - firstValue[1];
    })
    .slice(0, 10);

  let sortedObject = Object.fromEntries(sortedArray);

  return sortedObject;
}
module.exports = topTenPlayers;
