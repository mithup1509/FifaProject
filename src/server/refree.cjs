function matchesPerRefree(WorldCupMatches) {
  let refreeMatches = {};
  for (let val of WorldCupMatches) {
    let refreeName = val.Referee;
    if (refreeMatches[refreeName]) {
      refreeMatches[refreeName]++;
    } else {
      refreeMatches[refreeName] = 1;
    }
  }
  delete refreeMatches[""];
  return refreeMatches;

  //  let refreeMatches=   WorldCupMatches.reduce((acc,cur) => {
  // let refreeName=cur.Referee;
  // // console.log(refreeName);
  // if(acc[refreeName]){
  //     acc[refreeName]++;
  // }else{
  //     acc[refreeName]=1;
  // }
  // return acc;
  //     },{});
  //     delete refreeMatches[''];
  // return refreeMatches;
}
module.exports = matchesPerRefree;
