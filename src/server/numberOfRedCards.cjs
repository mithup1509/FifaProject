function numberOfRedCards(WorldCupPlayers, WorldCupMatches, year = 2014) {
  if (WorldCupMatches.length === 0 || WorldCupPlayers.length == 0) {
    return;
  }

  let initialscount = {};
  let redCardPlayerTeamInitial = WorldCupPlayers.filter((val) =>
    val.Event.includes("R")
  ).reduce((acc, cur) => {
    acc[cur.MatchID] = cur["Team Initials"];
    return acc;
  }, {});

  return WorldCupMatches.reduce((acc, cur) => {
    if (cur.Year == year && cur.Year !== undefined) {
      let matchid = cur.MatchID;
      if (redCardPlayerTeamInitial[matchid]) {
        if (initialscount[redCardPlayerTeamInitial[matchid]]) {
          initialscount[redCardPlayerTeamInitial[matchid]]++;
        } else {
          initialscount[redCardPlayerTeamInitial[matchid]] = 1;
        }

        if (redCardPlayerTeamInitial[matchid] === cur["Home Team Initials"]) {
          acc[cur["Home Team Name"]] =
            initialscount[redCardPlayerTeamInitial[matchid]];
        } else if (
          redCardPlayerTeamInitial[matchid] === cur["Away Team Initials"]
        ) {
          acc[cur["Away Team Name"]] =
            initialscount[redCardPlayerTeamInitial[matchid]];
        }
      }
    }
    return acc;
  }, {});
}
module.exports = numberOfRedCards;
