function matchesPerCity(WorldCupMatches) {
  if (WorldCupMatches.length === undefined) {
    return;
  }

  let matchesplayedinCity = WorldCupMatches.reduce(function (acc, cur) {
    if (acc[cur.City]) {
      acc[cur.City]++;
    } else {
      acc[cur.City] = 1;
    }
    return acc;
  }, {});
  delete matchesplayedinCity[""];
  return matchesplayedinCity;
}
module.exports = matchesPerCity;
